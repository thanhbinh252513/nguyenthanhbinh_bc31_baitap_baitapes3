const BASE_URL = "https://62b45bd7530b26da4cbcadb1.mockapi.io/Todolist";

export let Service = {
  getNote: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },

  addNote: (note) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: note,
    });
  },

  getCompleteNote: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },

  deleteNote: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
};
